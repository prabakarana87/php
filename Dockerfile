FROM php:7.1.1-apache

COPY apache-config.conf /etc/apache2/sites-enabled/000-default.conf
COPY index.php /index.php